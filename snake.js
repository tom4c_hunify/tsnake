class Point
{
    constructor(x, y)
    {
        this.x = x;
        this.y = y;
    }
}
class Snake
{
    constructor()
    {
        this.coords = [new Point(40, 10), new Point(30, 10), new Point(20, 10), new Point(10, 10), new Point(0, 10)];
    }

    move(dir, increaseLength)
    {
        let i = 0;
        let head = Object.assign({}, this.coords[0]);
        switch (dir)
        {
            case 37: head.x -= 10; break;
            case 38: head.y -= 10; break;
            case 39: head.x += 10; break;
            case 40: head.y += 10; break;
        }
        if (!increaseLength)
        {
            this.coords.pop();
        }
        this.coords.unshift(head);
    }

}

var snake;
var direction;
var canvas;
var timer;
var appleTimer;
var apple;
var score;

document.addEventListener("keydown", handleKeyPress);
document.addEventListener("DOMContentLoaded", onPageLoaded);

function onPageLoaded(evenet)
{
    canvas = document.getElementById("area");

    initGameArea();
}

function initGameArea()
{
    canvas.width = document.getElementById("gamefloor").offsetWidth;
    canvas.height = document.getElementById("gamefloor").offsetHeight;
    apple = new Point((Math.random() * (canvas.width-10))+10, (Math.random() * (canvas.height-10))+10);
    score = 0;
    snake = new Snake();
    render();
    direction = 39;
    timer = setInterval(gameLogic, 100);
}

function handleKeyPress(event)
{
    if (event.keyCode >= 37 && event.keyCode <= 40)
    {
        if ((direction + 2 != event.keyCode) && (direction - 2 != event.keyCode))
        {
            direction = event.keyCode;
        }
    }
    // r pressed
    else if (event.keyCode === 82)
    {
        if (timer)
        {
            clearInterval(timer);
            timer = null;
        }
        initGameArea();
    }
}


function gameLogic(event)
{
    let head = Object.assign({}, snake.coords[0]);
    if ((0 < head.x && head.x < canvas.width) && (0 < head.y && head.y < canvas.height))
    {
        let count = 0;
        let coordcount = snake.coords.filter(el =>
        {
            return el.x === head.x && el.y === head.y
        }).length;

        if (coordcount > 1)
        {
            console.error("Into own point");
            gameOver();
        }
        else
        {
            if ((head.x >= apple.x - 10) && (head.x <= apple.x + 10) &&
                (head.y >= apple.y - 10 && head.y <= apple.y + 10))
            {
                console.log("got it");
                score++;
                document.getElementById("score").innerText = score;
                apple = new Point(Math.random() * canvas.width, Math.random() * canvas.height);
                snake.move(direction, true);
            }
            else
            {
                snake.move(direction, false);
            }
            render();
        }
    }
    else
    {
        console.error("Out of bounds!");
        gameOver();
    }
}

function render()
{
    let ctx = canvas.getContext('2d');
    let i = 0;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawSnake(ctx);
    drawApples(ctx);
}

function drawSnake(context)
{
    for (i = 0; i < snake.coords.length; i++)
    {
        context.beginPath();
        context.rect(snake.coords[i].x-5, snake.coords[i].y-5, 10, 10);
        context.fillStyle = 'green';
        context.fill();
        context.stroke();
    }
}

function drawApples(context)
{

    context.beginPath();
    context.arc(apple.x, apple.y, 10, 0, 2 * Math.PI);
    context.fillStyle = 'red';
    context.fill();
    context.stroke();
}

function gameOver()
{
    alert("GAME OVER");
    clearInterval(timer);
    timer = null;
}